#!/bin/bash
wget -c --no-check-certificate http://ftp.us.debian.org/debian/pool/main/b/beautifulsoup/python-beautifulsoup_3.2.1-1_all.deb
wget -c --no-check-certificate  http://ftp.br.debian.org/debian/pool/main/g/gnome-python-desktop/python-wnck_2.32.0+dfsg-3_amd64.deb
wget -c --no-check-certificate  https://mirrors.wikimedia.org/debian/pool/main/libg/libgnomeui/libgnomeui-common_2.24.5-3.1_all.deb
wget -c --no-check-certificate  http://ftp.debian.org/debian/pool/main//libg/libgnome-keyring/libgnome-keyring-common_3.12.0-1_all.deb
wget -c --no-check-certificate  http://ftp.debian.org/debian/pool/main//libb/libbonoboui/libbonoboui2-common_2.24.5-4_all.deb
wget -c --no-check-certificate  http://ftp.debian.org/debian/pool/main/libg/libgnome/libgnome2-common_2.32.1-5_all.deb
wget -c --no-check-certificate  http://ftp.debian.org/debian/pool/main/g/gnome-vfs/libgnomevfs2-common_2.24.4-6.1_all.deb
wget -c --no-check-certificate  http://cdn-fastly.deb.debian.org/debian/pool/main/libb/libbonobo/libbonobo2-common_2.32.1-3_all.deb
wget -c --no-check-certificate  http://ftp.debian.org/debian/pool/main//libg/libgnome-keyring/libgnome-keyring0_3.12.0-1+b2_amd64.deb
wget -c --no-check-certificate  http://cdn-fastly.deb.debian.org/debian/pool/main/g/gnome-python-desktop/python-gnomekeyring_2.32.0+dfsg-3_amd64.deb
wget -c --no-check-certificate  http://ftp.br.debian.org/debian/pool/main/o/orbit2/liborbit-2-0_2.14.19-2+b1_amd64.deb
wget -c --no-check-certificate  http://ftp.br.debian.org/debian/pool/main/o/orbit2/liborbit2_2.14.19-2+b1_amd64.deb
wget -c --no-check-certificate  http://ftp.br.debian.org/debian/pool/main/p/pyorbit/python-pyorbit_2.24.0-7.2_amd64.deb
wget -c --no-check-certificate  http://cdn-fastly.deb.debian.org/debian/pool/main/g/gnome-python/python-gconf_2.28.1+dfsg-1.2_amd64.deb
wget -c --no-check-certificate  http://cdn-fastly.deb.debian.org/debian/pool/main/libb/libbonobo/libbonobo2-0_2.32.1-3+b1_amd64.deb
wget -c --no-check-certificate  http://ftp.debian.org/debian/pool/main/g/gnome-vfs/libgnomevfs2-0_2.24.4-6.1+b2_amd64.deb
wget -c --no-check-certificate  http://ftp.debian.org/debian/pool/main/libg/libgnome/libgnome-2-0_2.32.1-5+b1_amd64.deb
wget -c --no-check-certificate  http://ftp.br.debian.org/debian/pool/main/libb/libbonoboui/libbonoboui2-0_2.24.5-4_amd64.deb
wget -c --no-check-certificate  http://cdn-fastly.deb.debian.org/debian/pool/main/libg/libgnomeui/libgnomeui-0_2.24.5-3.1_amd64.deb
wget -c --no-check-certificate  http://cdn-fastly.deb.debian.org/debian/pool/main/g/gnome-python/python-gnome2_2.28.1+dfsg-1.2_amd64.deb
wget -c --no-check-certificate  https://launchpad.net/~screenlets/+archive/ubuntu/ppa/+files/screenlets_0.1.7_all.deb
wget -c --no-check-certificate  https://launchpad.net/~screenlets/+archive/ubuntu/ppa/+files/screenlets-pack-all_0.1.7.2-xenial_all.deb
gdebi -n python-beautifulsoup_3.2.1-1_all.deb
gdebi -n python-wnck_2.32.0+dfsg-3_amd64.deb
gdebi -n libgnomeui-common_2.24.5-3.1_all.deb
gdebi -n libgnome-keyring-common_3.12.0-1_all.deb
gdebi -n libbonoboui2-common_2.24.5-4_all.deb
gdebi -n libgnome2-common_2.32.1-5_all.deb
gdebi -n libgnomevfs2-common_2.24.4-6.1_all.deb
gdebi -n libbonobo2-common_2.32.1-3_all.deb
gdebi -n libgnome-keyring0_3.12.0-1+b2_amd64.deb
gdebi -n python-gnomekeyring_2.32.0+dfsg-3_amd64.deb
gdebi -n liborbit-2-0_2.14.19-2+b1_amd64.deb
gdebi -n liborbit2_2.14.19-2+b1_amd64.deb
gdebi -n python-pyorbit_2.24.0-7.2_amd64.deb
gdebi -n python-gconf_2.28.1+dfsg-1.2_amd64.deb
gdebi -n libbonobo2-0_2.32.1-3+b1_amd64.deb
gdebi -n libgnomevfs2-0_2.24.4-6.1+b2_amd64.deb
gdebi -n libgnome-2-0_2.32.1-5+b1_amd64.deb
gdebi -n libbonoboui2-0_2.24.5-4_amd64.deb
gdebi -n libgnomeui-0_2.24.5-3.1_amd64.deb
gdebi -n python-gnome2_2.28.1+dfsg-1.2_amd64.deb
gdebi -n screenlets_0.1.7_all.deb
gdebi -n screenlets-pack-all_0.1.7.2-xenial_all.deb
